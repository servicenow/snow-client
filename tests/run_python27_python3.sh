#!/usr/bin/env bash

# This file should be run from the root directory, i.e.
# tests/run_python27_python3.sh
# instead of
# ./run_python27_python3.sh

# Run the tests by auto discovering them (module: unittest)
# the parameter 'tests' denotes the path/folder where tests will be discovered

echo -e "python -m unittest discover tests"
python -m unittest discover tests
