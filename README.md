## Synopsis
Welcome to the **cern-snow-client** library, which facilitates the usage of the ServiceNow REST API with a CERN Single Sign On account, such as a CERN service account. Basic Authentication is also supported.

The library is Python 3 compatible. You can find a Python 2.6/2.7 compatible version in the **[Releases section](https://gitlab.cern.ch/servicenow/snow-client/-/releases)**.

The code can be found in package **[cern_snow_client](cern_snow_client)** and an example usage in **[examples/main.py](examples/main.py)**. For the latter to work properly, you must have a **`config.yaml`** file. You can, as a reference, rename either _[examples/config_basic.yaml](examples/config_basic.yaml)_ or _[examples/config_sso.yaml](examples/config_sso.yaml)_ to **`config.yaml`**. Choose any of the both based on the authentication method you would eventually use. More on this below.

## Access to the ServiceNow web service APIs
To have access to the ServiceNow APIs, please read first [KB0003644: Access to SNow APIs with a Service Account](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003644) and [KB0003521: Retrieving information from ServiceNow using REST and SOAP](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003521).

To request access for your service account, please fill in the form [Request access to SNow API for a Service Account](https://cern.service-now.com/service-portal/?id=sc_cat_item&name=snow-api-access&se=servicenow-application-support).

If for some reason you cannot use a CERN Service Account, please read [KB0003645: Access to SNow APIs with a ServiceNow Local Account](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003645).

## Motivation
The motivation for the development of this library is:
* Providing a common, reliable and documented Python client library for ServiceNow users, both in the IT Department and other CERN departments.
* Reducing implementation and support costs of integrations with ServiceNow at CERN.

## How to use

- First, you need a config file. You can use as an example any `.yaml` from the **[examples](examples)** folder _(a future idea later on is to have a config file generation tool.)_
- You can try the library interactively. Example below:

``` python
python
...
>>> from cern_snow_client.session import SnowRestSession
>>> s = SnowRestSession()
>>> s.load_config_file('config.yaml')  # points to cerntraining.service-now.com
>>> inc = Incident(s)
>>> if inc.get('c1c535ba85f45540adf94de5b835cd43'):
>>>     print(inc.short_description)
>>> inc2 = Incident(s)
>>> if inc2.get(('number', 'INC0426232')):
>>>     print(inc2.short_description)
```

- See more documentation for the class `SnowRestSession` at the [snow-client documentation website](https://snow-client-docs.web.cern.ch/snow-client-docs/cern_snow_client.html) and more examples in the file [`examples/main.py`](examples/main.py).

**Important**: with the included configuration files examples (_[examples/config_basic.yaml](examples/config_basic.yaml)_ or _[examples/config_sso.yaml](examples/config_sso.yaml)_) a cookie file `cookie.txt` and a token file `tokens.txt` will be generated _(at root level)_ as a cache to avoid reauthenticating between requests. These files have to be stored securely, as anyone with access to them could impersonate your account in ServiceNow or even another SSO-enabled CERN system. If you are just testing, be sure to delete the files after running `main.py` or any of your code testing.

## Installation

You can clone this git repository, using the URL at the top of this page, for example with:
``` bash
git clone https://:@gitlab.cern.ch:8443/servicenow/snow-client.git
```

You can then copy the package (folder) **`cern_snow_client`** into your Python project.

You can also download a `.zip`,`.tar.gz`, `tar.bz2` or `tar` file from the **[Releases](https://gitlab.cern.ch/servicenow/snow-client/-/releases)** page.

## Command Line Interface (CLI)

At the moment, there is no CLI around the library, other than using it interactively from the Python interpreter.

Developing a CLI is in the **[issue list](https://gitlab.cern.ch/servicenow/snow-client/-/issues)**. If you would like to contribute, please get in touch with us so as to coordinate the effort.

## Documentation

The folder **`docs`** contains some configuration files and *makefile*s to build the documentation of the library on demand. It works by using the python package [`sphinx`](https://pypi.org/project/Sphinx/). You need to have it installed. Among the different possible methods of generating the docs, these are some: _pdflatex_, _singlehtml_, plain text, _epub_. To see the complete list, if you are on Linux, locate yourself within `docs` and type `make help`.

Other than that, the current documentation is on the [snow-client documentation website](https://snow-client-docs.web.cern.ch/snow-client-docs/cern_snow_client.html).

## Unit tests

First, create a `tests/config_files/passwords.yaml` file. The following content needs to be added:
``` yaml
basic_good : <password of the account snow_client_basic_tests>
oauth_client_secret_good: <secret of the OAuth Client App 1b606d966a008380d686014a4cc61e42>
```
You can request the values of these passwords to the ServiceNow developers team (snow-devs@cern.ch) if you need to carry unit tests.

**Tests should never be done in the ServiceNow production instance `cern.service-now.com`**. The target instance is controlled by the **`instance`** parameter in the `.yaml` configuration file.

To run the unit tests: while in the root of the project, please run `tests/run_python26.sh` or `tests/run_python27_python3.sh`.

The SSO+OAuth tests will only work in an environment with the tool `cern-get-sso-cookie` (e.g. Scientific Linux CERN or CERN CentOS).

Tested environments are _lxplus.cern.ch_ (for Python 2.7.5) and locally in a Python 3.9.5 setup.

## Future work

Please check our [Issue List](https://gitlab.cern.ch/servicenow/snow-client/issues) to see the planned improvements.

Any ideas or contributions are welcome. Please get in touch with us.

## Contributors

James Clerc	james.clerc@cern.ch james.clerc@epitech.eu

David Martin Clavo david.martin.clavo@cern.ch

Aitor De Blas aitor.de.blas.granja@cern.ch aitor.db@opendeusto.es